const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports= {
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // "2"
        const greenHex = green.toString(16);
        const blueHex  = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex);
    },
    hexToRgb: (hex) => {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if(result){
        var redRgb= parseInt(result[1], 16);
        var greenRgb= parseInt(result[2], 16);
        var blueRgb= parseInt(result[3], 16);
      return "("+ +redRgb + "," + +greenRgb + "," + +blueRgb + ")";
  } 
          
          
    }
}