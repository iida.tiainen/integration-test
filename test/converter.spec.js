// TDD- Test driven development- unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');

describe("Color code converter", () => {
    describe("RGB to Hex conversion" , () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000 (0-f)
            const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    })
    describe("Hex to RGB", () => {
        it("converts the basic colors", () => {
            const redRgb = converter.hexToRgb("ff0000"); //255, 0, 0)
            const greenRgb = converter.hexToRgb("00ff00"); //0, 255, 0)
            const blueRgb = converter.hexToRgb("0000ff"); //0, 0, 255)
            

            expect(redRgb).to.equal("(255,0,0)")
            expect(greenRgb).to.equal("(0,255,0)")
            expect(blueRgb).to.equal("(0,0,255)")
        })
    })
})